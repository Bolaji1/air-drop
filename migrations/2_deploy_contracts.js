var SafeMath = artifacts.require("../contracts/LegalBot.sol");
var LegalBot = artifacts.require("../contracts/LegalBot.sol");
var Airdrop = artifacts.require("../contracts/Airdrop.sol");
var fs  = require('fs');

module.exports = function(deployer, networks, accounts) {
    deployer.deploy(SafeMath).then(function(){
        deployer.link(SafeMath, [LegalBot]);
        deployer.deploy(LegalBot)
        .then(() => { 
            deployer.deploy(Airdrop);
        });
    });
};