import { Component, OnInit } from '@angular/core';
import { Web3Service } from './shared/web3.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TokenContractService } from './shared/token-contract.service';
import * as Web3 from 'web3';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  web3: Web3;
  accounts: string[];
  allowValForm: FormGroup;
  valForm: FormGroup;
  allowance: number;

  constructor(private fb: FormBuilder, public tokenContractService: TokenContractService, private web3Service: Web3Service) {
    this.allowValForm = this.fb.group({
      'amount': [null, Validators.required]
    });

    this.valForm = this.fb.group({
      'addresses': [null, Validators.required],
      'amount': [null, Validators.required]
    });
  }

  async ngOnInit() {
    this.web3 = this.web3Service.getWeb3();
    this.accounts = await this.getAccounts();
    this.web3.eth.defaultAccount = this.accounts[0];
    await this.initAllowance();
  }

  async initAllowance() {
    this.allowance = await this.tokenContractService.getAllowance(this.web3.eth.defaultAccount) / Math.pow(10, 18);
  }

  async submitAllowForm($ev, value: any) {
    $ev.preventDefault();
    for (let c in this.allowValForm.controls) {
      this.allowValForm.controls[c].markAsTouched();
    }
    if (this.allowValForm.valid) {
      const amount = Number(this.allowValForm.value.amount);
      const result = await this.tokenContractService.approve(this.web3.eth.defaultAccount, amount);
      if (result) {
        alert('Added airdrop aprove ');
        await this.initAllowance();
      } else {
        console.log('Could not aprove Airdrop.');
        alert('Token contract approve failed');
      }
    }
  }

  async submitForm($ev, value: any) {
    $ev.preventDefault();
    for (let c in this.valForm.controls) {
      this.valForm.controls[c].markAsTouched();
    }
    if (this.valForm.valid) {
      const addreses = this.valForm.value.addresses.replace(/\r?\n|\r/g, '').split(',').filter(s => !!s).map(s => s.trim());
      const amount = Number(this.valForm.value.amount);
      await this.tokenContractService.airdrop(this.web3.eth.defaultAccount, addreses, amount);
      alert('Airdrop completed');
      this.valForm.reset();
    }
  }

  private getAccounts(): Promise<string[]> {
    const promise = new Promise<string[]>((resolve, reject) => {
      this.web3.eth.getAccounts((err, accounts) => {
        if (err) {
          alert('Cannot load ethereum accounts');
          console.log('Cannot load ethereum accounts');
          reject();
        } else {
          resolve(accounts);
        }
      });
    });
    return promise;
  }
}
