import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TokenContractService } from './shared/token-contract.service';
import { Web3Service } from './shared/web3.service';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [TokenContractService, Web3Service],
  bootstrap: [AppComponent]
})
export class AppModule { }
