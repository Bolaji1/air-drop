import { Injectable } from '@angular/core';

import * as Web3 from 'web3';

@Injectable()
export class Web3Service {

  getWeb3(): Web3 {
    return window.web3;
  }
}