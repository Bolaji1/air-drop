import { Injectable } from '@angular/core';
import { Web3Service } from './web3.service';
const tokenData = require('../../../build/contracts/LegalBot.json');
const airdropData = require('../../../build/contracts/Airdrop.json');
import * as Web3 from 'web3';

export type Callback = (error: any, result: any) => void;
export type AproveExecutor = (spender: string, amount: string, { from: string, gas: number }, callbak: Callback) => boolean;
export type AllowanceExecutor = (owner: string, spender: string, callbak: Callback) => number;
export type AirdropExecutor = (token: string, _addresses: string[], amount: string, { from: string }, callbak: Callback) => void;

@Injectable()
export class TokenContractService {
  tokenAddress = '0xB2E285cb55667AfE30F6CC39B9454f3Bf35EEe3A';
  airdropAddress = '0x71197081B67Fabc6C774B03FA1E16644b4E7E758';
  tokenInstance: any;
  airdropInstance: any;
  web3: Web3;

  constructor(private web3Service: Web3Service) {
    this.web3 = this.web3Service.getWeb3();
    const tokenAbi: Web3.AbiDefinition[] = tokenData.abi;
    const airdropAbi: Web3.AbiDefinition[] = airdropData.abi;

    const tokenContract = this.web3.eth.contract(tokenAbi);
    this.tokenInstance = tokenContract.at(this.tokenAddress);

    const airdropContract = this.web3.eth.contract(airdropAbi);
    this.airdropInstance = airdropContract.at(this.airdropAddress);
  }

  async getAllowance(owner: string): Promise<number> {
    return await this.toAllowancePromise(owner, this.airdropAddress, this.tokenInstance.allowance, 'Cannot get allowance');
  }

  async approve(owner: string, amount: number): Promise<boolean> {
    return this.toAprovePromise(owner, this.toTokenPart(amount), this.tokenInstance.approve, 'Cannot approve');
  }

  airdrop(owner: string, addresses: string[], amount: number): Promise<void> {
    return this.toAirdropPromise(owner, addresses, this.toTokenPart(amount), this.airdropInstance.drop, 'Cannot Airdrop');
  }

  private toTokenPart(amount: number) {
    return (amount * Math.pow(10, 18)).toString(10);
  }

  private toTokenPartNumb(amount: number) {
    return (amount * Math.pow(10, 18));
  }

  private toAirdropPromise<T>(owner: string, addresses: string[], amount: string,
    executor: AirdropExecutor, message: string = '', mapper: (any) => T = null): Promise<T> {
    console.log('addresses: ' + addresses);
    console.log('amount: ' + amount);
    const promise = new Promise<T>((resolve, reject) => {
      executor(this.tokenAddress, addresses, amount, {from: owner}, (error, result) => {
        if (error) {
          console.log('toAirdropPromise: ' + error);
          alert(error);
          reject(error);
        } else {
          console.log('toAirdropPromise: success');
          resolve(result);
        }
      });
    });
    return promise;
  }

  private toAprovePromise<T>(owner: string, amount: string,
    executor: AproveExecutor, message: string = '', mapper: (any) => T = null): Promise<boolean> {
    const promise = new Promise<boolean>((resolve, reject) => {
      executor(this.airdropAddress, amount, {from: owner, gas: 210000}, (error, result) => {
        if (error) {
          console.log('toAprovePromise: ' + error);
          reject(error);
        } else {
          console.log('toAprovePromise: success');
          resolve(result);
        }
      });
    });
    return promise;
  }

  private toAllowancePromise<T>(owner: string, spender: string,
    executor: AllowanceExecutor, message: string = '', mapper: (any) => T = null): Promise<number> {
    const promise = new Promise<number>((resolve, reject) => {
      executor(owner, spender, (error, result) => {
        if (error) {
          console.log('toAllowancePromise: ' + error);
          reject(error);
        } else {
          console.log('toAllowancePromise: success');
          resolve(result);
        }
      });
    });
    return promise;
  }
}
