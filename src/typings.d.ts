/// <reference path="../node_modules/@types/node/index.d.ts" />
/// <reference path="../node_modules/web3-typescript-typings/index.d.ts" />

declare const require: any;

import * as Web3 from 'web3';

/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

/* Web 3 */
declare global {
  interface Window { web3: Web3; }
}

declare module "*.json" {
  const value: any;
  export default value;
}